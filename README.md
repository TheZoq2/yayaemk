# YAYAEMK

Firmware for my keyboard which is slightly derived from the excellent [yaemk](https://karlk90.github.io/yaemk-split-kb/).

The bulk of the heavy lifting is done by [keyberon](https://github.com/TeXitoi/keyberon).

