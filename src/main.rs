#![no_main]
#![no_std]

// set the panic handler
use panic_rtt_target as _;

use core::convert::Infallible;
// use embedded_hal::digital::v2::InputPin;
use keyberon::debounce::Debouncer;
use keyberon::key_code::KbHidReport;
use keyberon::layout::{Event, Layout};
use keyberon::matrix::Matrix;
use nb::block;
use rtic::app;
use rtt_target::rprintln;
use stm32f1xx_hal::gpio::{EPin, Input, Output, PullUp, PushPull};
use stm32f1xx_hal::prelude::*;
use stm32f1xx_hal::usb::{Peripheral, UsbBusType};
use stm32f1xx_hal::{pac, serial, timer};
use usb_device::bus::UsbBusAllocator;
use usb_device::class::UsbClass as _;
use usb_device::device::UsbDeviceState;

mod encoder;
mod layout;
mod backlight;

type UsbClass = keyberon::Class<'static, UsbBusType, ()>;
type UsbDevice = usb_device::device::UsbDevice<'static, UsbBusType>;

trait ResultExt<T> {
    fn get(self) -> T;
}
impl<T> ResultExt<T> for Result<T, Infallible> {
    fn get(self) -> T {
        match self {
            Ok(v) => v,
            Err(e) => match e {},
        }
    }
}

#[app(device = stm32f1xx_hal::pac, peripherals = true, dispatchers = [CAN_SCE])]
mod app {
    use backlight::LED_COUNT;
    use backlight::BacklightEffect;
    use encoder::Direction;
    use smart_leds::{
        hsv::{hsv2rgb, Hsv},
        SmartLedsWrite, RGB,
    };
    use stm32f1xx_hal::{
        gpio,
        timer::{CounterHz, Timer},
    };
    use ws2812_timer_delay::Ws2812;

    use crate::{encoder::Rotary, backlight::{key_idx_from_coord, FadingKeys}};

    use super::*;

    #[shared]
    struct Shared {
        usb_dev: UsbDevice,
        usb_class: UsbClass,
        layout: layout::LayoutT,
        led_state: [RGB<u8>; backlight::LED_COUNT],
        scratchpad: [i16; backlight::KEY_COUNT],
        led_effect: FadingKeys,
    }

    #[local]
    struct Local {
        matrix: Matrix<EPin<Input<PullUp>>, EPin<Output<PushPull>>, 8, 5>,
        debouncer: Debouncer<[[bool; 8]; 5]>,
        timer: CounterHz<pac::TIM2>,
        led_tick_timer: CounterHz<pac::TIM1>,
        transform: fn(Event) -> Event,
        tx: serial::Tx<pac::USART3>,
        rx: serial::Rx<pac::USART3>,
        rotary: Rotary<gpio::PA3<Input<gpio::Floating>>, gpio::PA4<Input<gpio::Floating>>>,
        rotary_last_coord: Option<(u8, u8)>,
        buf: [u8; 4],
        leds: Ws2812<CounterHz<pac::TIM3>, gpio::PA10<Output<PushPull>>>,
        led_ticks: u32,
        timeout: bool,
        is_left: bool,
    }

    #[init(local = [bus: Option<UsbBusAllocator<UsbBusType>> = None])]
    fn init(c: init::Context) -> (Shared, Local, init::Monotonics) {
        rtt_target::rtt_init_print!();

        // static mut USB_BUS: Option<UsbBusAllocator<UsbBusType>> = None;

        let mut flash = c.device.FLASH.constrain();
        let rcc = c.device.RCC.constrain();
        let mut afio = c.device.AFIO.constrain();

        // set 0x424C in DR10 for dfu on reset
        // let bkp = rcc
        //     .bkp
        //     .constrain(c.device.BKP, &mut rcc.apb1, &mut c.device.PWR);
        // bkp.write_data_register_low(9, 0x424C);

        rprintln!("Pre clock set");

        let clocks = rcc
            .cfgr
            // .use_hse(8.MHz())
            .sysclk(48.MHz()) // can also run at 72MHz
            .pclk1(24.MHz())
            .pclk2(24.MHz())
            .freeze(&mut flash.acr);

        // NOTE: Not doing this check is very not recommended. Unfortunately something on my board
        // means that the HSE does not come online. I'm therefore runnig USB on the internal clock
        // which is not officially supported. However, in my testing so far it seems to work out
        // alright...
        // assert!(clocks.usbclk_valid());

        let mut gpioa = c.device.GPIOA.split();
        let mut gpiob = c.device.GPIOB.split();
        let mut gpioc = c.device.GPIOC.split();

        // USB is on PA11 (D-) and PA12 (D+)
        // Pull the D+ pin down to send a RESET condition to the USB bus.
        let mut usb_dp = gpioa.pa12.into_push_pull_output(&mut gpioa.crh);
        usb_dp.set_low();
        cortex_m::asm::delay(clocks.sysclk().raw() / 100);

        let usb_dm = gpioa.pa11;
        let usb_dp = usb_dp.into_floating_input(&mut gpioa.crh);

        let usb = Peripheral {
            usb: c.device.USB,
            pin_dm: usb_dm,
            pin_dp: usb_dp,
        };

        *c.local.bus = Some(UsbBusType::new(usb));
        let usb_bus = c.local.bus.as_ref().unwrap();

        let usb_class = keyberon::new_class(usb_bus, ());
        let mut usb_dev = keyberon::new_device(usb_bus);

        // Set up the matrix scan timer, polls at 1kHz (1000 times a second/every 1ms)
        let mut timer = c.device.TIM2.counter_hz(&clocks);
        // let mut timer = timer::Timer::new(c.device.TIM3, &clocks);
        timer.start(1000.Hz()).unwrap();
        timer.listen(timer::Event::Update);

        let mut led_tick_timer = c.device.TIM1.counter_hz(&clocks);
        led_tick_timer.start(100.Hz()).unwrap();
        led_tick_timer.listen(timer::Event::Update);

        // USART3
        let tx = gpiob.pb10.into_alternate_push_pull(&mut gpiob.crh);
        // Take ownership over pb11
        let rx = gpiob.pb11;

        let serial = serial::Serial::usart3(
            c.device.USART3,
            (tx, rx),
            &mut afio.mapr,
            serial::Config::default().baudrate(115200.bps()),
            clocks,
        );

        let (tx, mut rx) = serial.split();
        rx.listen();

        macro_rules! gpio_input {
            ($gpio:ident, $cr:ident, $field:ident) => {
                $gpio.$field.into_pull_up_input(&mut $gpio.$cr).erase()
            };
        }

        macro_rules! gpio_output {
            ($gpio:ident, $cr:ident, $field:ident) => {
                $gpio.$field.into_push_pull_output(&mut $gpio.$cr).erase()
            };
        }

        let matrix = Matrix::new(
            [
                gpio_input!(gpiob, crh, pb15), // col8 (7) b
                gpio_input!(gpiob, crh, pb14), // col7 (6)
                gpio_input!(gpiob, crh, pb13), // col6 (5) b
                gpio_input!(gpiob, crl, pb2),  // col5 (4) b
                gpio_input!(gpiob, crl, pb1),  // col4 (3)
                gpio_input!(gpiob, crl, pb0),  // col3 (2)
                gpio_input!(gpioa, crl, pa7),  // col2 (1) b
                gpio_input!(gpioa, crl, pa2),  // col1 (0) b
            ],
            [
                gpio_output!(gpioa, crh, pa8),  // row 1 (0)
                gpio_output!(gpiob, crh, pb12), // row 2 (1) b
                gpio_output!(gpioa, crl, pa6),  // row 3 (2)
                gpio_output!(gpioa, crl, pa5),  // row 4 (3)
                gpio_output!(gpioa, crl, pa1),  // row 5 (4)
            ],
        );

        #[cfg(not(feature="dcs_controller"))]
        let handedness_pin = gpioc.pc13.into_pull_up_input(&mut gpioc.crh);
        #[cfg(not(feature="dcs_controller"))]
        let is_left = handedness_pin.is_high();
        #[cfg(feature="dcs_controller")]
        let is_left = true;

        rprintln!("is_left: {}", is_left);

        let transform: fn(Event) -> Event = if is_left {
            |e| e
        } else {
            |e| e.transform(|i, j| (i, 15 - j))
        };

        let rotary = Rotary::new(gpioa.pa3, gpioa.pa4);

        let mut led_timer = Timer::new(c.device.TIM3, &clocks).counter_hz();
        led_timer.start(3.MHz()).unwrap();
        let leds = Ws2812::new(led_timer, gpioa.pa10.into_push_pull_output(&mut gpioa.crh));

        rprintln!("Up and running");

        (
            Shared {
                usb_dev,
                usb_class,
                layout: Layout::new(&crate::layout::LAYERS),
                led_state: [RGB { r: 0, g: 0, b: 0 }; LED_COUNT],
                scratchpad: [0; backlight::KEY_COUNT],
                led_effect: FadingKeys{
                    hue: 0.03,
                    base_hue: -0.18,
                    base_val: 0.1,
                    decay_time: 255,
                },
            },
            Local {
                timer,
                debouncer: Debouncer::new([[false; 8]; 5], [[false; 8]; 5], 5),
                matrix: matrix.get(),
                transform,
                tx,
                rx,
                rotary,
                rotary_last_coord: None,
                buf: [0; 4],
                leds,
                led_tick_timer,
                led_ticks: 0,
                timeout: false,
                is_left,
            },
            init::Monotonics(),
        )
    }

    #[task(binds = USART3, priority = 5, local = [rx, buf])]
    fn rx(c: rx::Context) {
        while let Ok(b) = c.local.rx.read() {
            c.local.buf.rotate_left(1);
            c.local.buf[3] = b;

            if c.local.buf[3] == b'\n' {
                if let Ok(event) = de(&c.local.buf[..]) {
                    handle_event::spawn(event).unwrap();
                }
            }
        }
    }

    #[task(binds = USB_HP_CAN_TX, priority = 4, shared = [usb_dev, usb_class])]
    fn usb_tx(c: usb_tx::Context) {
        (c.shared.usb_dev, c.shared.usb_class)
            .lock(|usb_dev, usb_class| usb_poll(usb_dev, usb_class));
    }

    #[task(binds = USB_LP_CAN_RX0, priority = 4, shared = [usb_dev, usb_class])]
    fn usb_rx(c: usb_rx::Context) {
        (c.shared.usb_dev, c.shared.usb_class)
            .lock(|usb_dev, usb_class| usb_poll(usb_dev, usb_class));
    }

    #[task(priority = 3, capacity = 8, shared = [layout])]
    fn handle_event(mut c: handle_event::Context, event: Event) {
        c.shared.layout.lock(|layout| layout.event(event));
    }

    #[task(priority = 3, shared = [usb_dev, usb_class, layout], local=[timeout])]
    fn tick_keyberon(mut c: tick_keyberon::Context) {
        let _tick = c.shared.layout.lock(|layout| layout.tick());
        if c.shared.usb_dev.lock(|d| d.state()) != UsbDeviceState::Configured {
            return;
        }
        let report: KbHidReport = c.shared.layout.lock(|layout| layout.keycodes().collect());

        // if report.as_bytes().iter().any(|v| *v != 0) {
        //     rprintln!("{:?}", report)
        // }

        if c.shared
            .usb_class
            .lock(|k| k.device_mut().set_keyboard_report(report.clone()))
            || *c.local.timeout
        {
            let mut retry_count = 0;
            let to_write = report.as_bytes();
            let mut write_index = 0;
            *c.local.timeout = false;
            loop {
                match c.shared.usb_class.lock(|k| k.write(report.as_bytes())) {
                    Ok(count) => {
                        write_index += count;
                        if write_index == to_write.len() {
                            break;
                        }

                        retry_count += 1;

                        if retry_count == 1024 {
                            rprintln!("Full timeout");
                            *c.local.timeout = true;
                            break;
                        }
                    }
                    Err(_) => {
                        rprintln!("USB error {}");
                    }
                }
            }
        }
    }

    #[task(
        binds = TIM2,
        priority = 2,
        local = [matrix, debouncer, timer, transform, tx, rotary, rotary_last_coord],
    )]
    fn tick(c: tick::Context) {
        // If we need to send a release event for the encoder do that, otherwise
        // send a press event if the encoder is spun
        let encoder_event =
            if let Some((x, y)) = c.local.rotary_last_coord.clone() {
                *c.local.rotary_last_coord = None;
                Some(Event::Release(x, y))
            } else {
                match c.local.rotary.update() {
                    Some(Direction::Clockwise) => {
                        *c.local.rotary_last_coord = Some((0, 7));
                        Some(Event::Press(0, 7))
                    }
                    Some(Direction::CounterClockwise) => {
                        *c.local.rotary_last_coord = Some((1, 7));
                        Some(Event::Press(1, 7))
                    }
                    None => None,
                }
            };

        c.local.timer.wait().ok();

        for event in c
            .local
            .debouncer
            .events(c.local.matrix.get().get())
            .chain(encoder_event.into_iter())
            .map(c.local.transform)
        {
            for &b in &ser(event) {
                block!(c.local.tx.write(b)).get();
            }
            handle_event::spawn(event).unwrap();
            led_handle_event::spawn(event).unwrap();
        }
        tick_keyberon::spawn().unwrap();
    }

    #[task(
        binds=TIM1_UP,
        priority = 1,
        shared = [led_state, scratchpad, led_effect],
        local = [led_tick_timer, led_ticks]
    )]
    fn led_tick(c: led_tick::Context) {
        c.local.led_tick_timer.wait().ok();


        // let ticks = (*c.local.led_ticks / 6) % 39;
        *c.local.led_ticks += 1;

        (c.shared.led_effect, c.shared.scratchpad, c.shared.led_state)
            .lock(|effect, scratchpad, colors| {
                effect.tick(scratchpad, colors);
            });
        /*
        c.shared.led_state.lock(|state| {
            let black = RGB { r: 0, g: 0, b: 0 };

            let color = hsv2rgb(Hsv {
                hue: (ticks >> 2) as u8,
                sat: 255,
                val: 255,
            });
            let purple = color;

            let (x, y) = (ticks % 8, ticks/8);

            *state = [black;42];
            if let Some(idx) = led_from_coord((x as u8, y as u8)) {
                state[idx] = purple;
            }

            // *state = [
            //     purple,
            //     black,
            //     purple,
            //     purple,
            //     purple,
            //     black,
            //     purple,
            //     purple,
            //     black,
            //     purple,
            //     purple,
            //     purple,
            //     purple,
            //     black,
            //     purple,
            //     purple,
            //     purple,
            //     purple,
            //     purple,
            //     purple,
            //     purple,
            //     purple,
            //     purple,
            //     purple,
            //     purple,
            //     purple,
            //     purple,
            //     black,
            //     purple,
            //     purple,
            //     black,
            //     purple,
            //     purple,
            //     purple,
            //     black,
            //     purple,
            //     purple,
            //     purple,
            //     purple,
            //     purple,
            //     purple,
            //     purple
            // ]
        });
        */

        led_draw::spawn().unwrap();
    }

    #[task(priority = 3, shared=[led_state], local = [leds])]
    fn led_draw(mut c: led_draw::Context) {
        c.local
            .leds
            .write(
                c.shared
                    .led_state
                    .lock(|led_state| led_state.clone().into_iter()),
            )
            .unwrap();
    }

    // TODO: The priority of this could be lower if I can figure out 
    // a priority split that works
    #[task(priority = 3, capacity = 8, shared = [led_effect, scratchpad], local = [is_left])]
    fn led_handle_event(c: led_handle_event::Context, event: Event) {
        let x_transform = if *c.local.is_left {
            |x| x
        }
        else {
            |x| 15-x
        };

        (c.shared.led_effect, c.shared.scratchpad).lock(|e, s| {
            e.on_event(x_transform, &event, s)
        });
    }

}

fn de(bytes: &[u8]) -> Result<Event, ()> {
    match *bytes {
        [b'P', i, j, b'\n'] => Ok(Event::Press(i, j)),
        [b'R', i, j, b'\n'] => Ok(Event::Release(i, j)),
        _ => Err(()),
    }
}
fn ser(e: Event) -> [u8; 4] {
    match e {
        Event::Press(i, j) => [b'P', i, j, b'\n'],
        Event::Release(i, j) => [b'R', i, j, b'\n'],
    }
}

fn usb_poll(usb_dev: &mut UsbDevice, keyboard: &mut UsbClass) {
    if usb_dev.poll(&mut [keyboard]) {
        keyboard.poll();
    }
}
