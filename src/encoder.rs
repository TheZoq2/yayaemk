use embedded_hal::digital::v2::InputPin;
use rtt_target::rprintln;

pub enum Direction {
    Clockwise,
    CounterClockwise,
}

pub struct Rotary<A: InputPin, B: InputPin> {
    a: A,
    b: B,

    pub prev: (bool, bool)
}


impl<A: InputPin, B: InputPin> Rotary<A, B> {
    pub fn new(a: A, b: B) -> Self {
        Self {
            prev: (a.is_high().ok().unwrap(), b.is_high().ok().unwrap()),
            a,
            b,
        }
    }

    pub fn update(&mut self) -> Option<Direction> {
        let a_high = self.a.is_high().ok().unwrap();
        let b_high = self.b.is_high().ok().unwrap();
        let new = (a_high, b_high);

        let result = if new != self.prev {
            if new.0 != self.prev.0 {
                if new.0 == self.prev.1 {
                    Some(Direction::CounterClockwise)
                }
                else {
                    Some(Direction::Clockwise)
                }
            }
            else {
                None
            }
        }
        else {
            None
        };

        self.prev = new;
        result
    }
}
