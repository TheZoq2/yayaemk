use keyberon::action::{k, l};
use keyberon::key_code::KeyCode::*;

type Action = keyberon::action::Action<()>;


#[cfg(not(feature = "dcs_controller"))]
mod keymap {
    use super::*;

    const L_ENC_LEFT: Action = k(Left);
    const L_ENC_RIGHT: Action = k(Right);
    const R_ENC_LEFT: Action =  k(Up);
    const R_ENC_RIGHT: Action = k(Down);

    const L_SYM: Action = l(1);
    const L1_SHIFT: Action = Action::MultipleActions(&[k(LShift), L_SYM]);
    const L_WM: Action = Action::MultipleActions(&[k(LGui), l(2)]);
    const L_F: Action = l(3);
    const L_MOVE: Action = l(4);
    const L_SPECIAL: Action = l(5);

    const LAYER_COUNT: usize = 6;
    pub type LayerT = keyberon::layout::Layers<16, 5,  LAYER_COUNT,   ()>;
    pub type LayoutT = keyberon::layout::Layout<16, 5, LAYER_COUNT, ()>;

    #[rustfmt::skip]
    pub static LAYERS: LayerT = keyberon::layout::layout! {
        {
            [Grave  Kb1   Kb2 Kb3 Kb4    Kb5     {L_F} {L_ENC_LEFT}  {R_ENC_LEFT}  {L_F}       Kb6        Kb7   Kb8  Kb9    Kb0   W],
            [Tab    Q     W   E   R      T       W     {L_ENC_RIGHT} {R_ENC_RIGHT} Delete      Y          U     I    O      P     BSpace],
            [Escape A     S   D   F      G       W     W             W             {L_SPECIAL} H          J     K    L      ;     Enter],
            [LShift Z     X   C   V      B       W     W             '\\'          W           N          M     ,    .      /     '\''],
            [LCtrl  LAlt  t   t   {L_WM} {L_SYM} Space G             W             {L_MOVE}    {L1_SHIFT} RAlt  RShift    t      RCtrl RShift],
        }
        {           //    L_SYM
            [t      t     t   t   t      t       t     t             t             t           t          t     t    t      t     t],
            [t      Kb1   Kb2 Kb3 Kb4    Kb5     W     t             t             t           Kb6        Kb7   Kb8  Kb9    Kb0   t],
            [t      Minus t   t   t      t       t     t             t             t           t          Equal '['  ']'    '\\'  t],
            [t      t     t   t   t      t       t     t             t             t           t          t     t    t      t     t],
            [t      t     t   t   t      t       t     t             t             t           t          t     t    t      t     t],
        }
        {           //    LWM
            [t      t     t   t   t      t       t     t             t             t           t          t     t    t      t     t],
            [t      Kb1   Kb2 Kb3 Kb4    Kb5     t     t             t             t           Kb6        Kb7   Kb8  Kb9    Kb0   t],
            [t      Q     t   T   R      F       t     t             t             t           t          t     t    t      t     t],
            [t      t     t   t   B      Space   t     t             t             t           t          t     t    t      t     t],
            [t      t     t   t   t      t       t     t             t             t           t          t     t    t      t     t],
        }
        {           //    L_F
            [t      F1    F2  F3  F4     F5      t     t             t             t           F6         F7    F8   F9     F10   F11],
            [t      t     t   t   t      t       t     t             t             t           t          t     t    t      t     t],
            [t      t     t   t   t      t       t     t             t             t           t          t     t    t      t     t],
            [t      t     t   t   t      t       t     t             t             t           t          t     t    t      t     t],
            [t      t     t   t   t      t       t     t             t             t           t          t     t    t      t     t],
        }
        {           //    L_MOVE
            [t      t     t   t   t      t       t     t             t             t           t          t     t    t      t     t],
            [t      t     t   t   t      t       t     t             t             t           t          t     Home t      t     t],
            [t      End   t   t   t      t       t     t             t             t           Left       Down  Up   Right  t     t],
            [t      t     t   t   t      t       t     t             t             t           t          t     t    t      t     t],
            [t      t     t   t   t      t       t     t             t             t           t          t     t    t      t     t],
        }
        {           //    L_SPECIAL
            [t      t     t   t   t      t       t     t             t             t           t          t     t    t      t     t],
            [t      t     t   t   t      t       t     t             t             t           Kp7        Kp8   Kp9  Insert Home  PgUp],
            [t      t     t   t   t      t       t     t             t             t           Kp4        Kp5   Kp6  Delete End   PgDown],
            [t      t     t   t   t      t       t     t             t             t           Kp1        Kp2   Kp3  t      t     t],
            [t      t     t   t   t      t       t     t             t             t           Kp0        t     t    t      t     t],
        }
        //          {
        //          [t    t   t   t      t       t     t t    t           t          t     t    t      t     t t],
        //          [t    t   t   t      t       t     t t    t           t          t     t    t      t     t t],
        //          [t    t   t   t      t       t     t t    t           t          t     t    t      t     t t],
        //          [t    t   t   t      t       t     t t    t           t          t     t    t      t     t t],
        //          [t    t   t   t      t       t     t t    t           t          t     t    t      t     t t],
        //          }
    };
}


#[cfg(feature   ="dcs_controller")]
mod keymap {
    use super::*;


    const L_ENC_LEFT: Action = k(MediaScrollDown);
    const L_ENC_RIGHT: Action = k(MediaScrollUp);
    const R_ENC_LEFT: Action =  k(Up);
    const R_ENC_RIGHT: Action = k(Down);

    const LAYER_COUNT: usize = 6;
    pub type LayerT = keyberon::layout::Layers<16, 5,  LAYER_COUNT,   ()>;
    pub type LayoutT = keyberon::layout::Layout<16, 5, LAYER_COUNT, ()>;

    const L_F_KEYS: Action = l(1);
    const L_NUMPAD: Action = l(2);
    const L_NUMBERS: Action = l(3);
    const L_RIGHT: Action = l(4);

    const NO: Action = Action::NoOp;

    // const L1_SHIFT: Action = Action::MultipleActions(&[k(LShift), L_SYM]);
    // const L_WM: Action = Action::MultipleActions(&[k(LGui), l(2)]);
    // const L_F: Action = l(3);
    // const L_MOVE: Action = l(4);
    // const L_SPECIAL: Action = l(5);

    #[rustfmt::skip]
    pub static LAYERS: LayerT = keyberon::layout::layout! {

        {
            [{L_F_KEYS} Kb1   Kb2    Kb3  Kb4       Kb5         {L_F_KEYS} {L_ENC_LEFT}  {R_ENC_LEFT}  /**/t t t    t t t W],
            [Tab        Q     W      E    R         T           W          {L_ENC_RIGHT} {R_ENC_RIGHT} /**/t t U    I O P t],
            [Escape     A     S      D    F         G           W          W             W             /**/t H J    K L ; t],
            [LShift     Z     X      C    V         B           W          W             '\\'          /**/t N M    , . / t],
            [LCtrl      LAlt  t      t    {L_RIGHT} {L_NUMBERS} {L_NUMPAD} Bslash        W             /**/t t t    t t t t],
        }
        {               //    L_F_KEYS
            [t          F1    F2     F3   F4        F5          t          t             t             /**/t t t    t t t W],
            [t          F6    F7     F8   F9        F10         F11        t             t             /**/t t U    I O P t],
            [t          Minus {NO}   {NO} {NO}      {NO}        t          t             t             /**/t H J    K L ; t],
            [t          {NO}  {NO}   {NO} {NO}      {NO}        t          t             t             /**/t N M    , . / t],
            [t          {NO}  {NO}   {NO} {NO}      {NO}        t          t             t             /**/t t t    t t t t],
        }
        {               //    L_NUMPAD
            [t          {NO}  {NO}   {NO} Kp0       {NO}        t          KpAsterisk    t             /**/t t t    t t t W],
            [Insert     Home  PgUp   Kp9  Kp8       Kp7         t          KpMinus       t             /**/t t U    I O P t],
            [Delete     End   PgDown Kp4  Kp5       Kp6         t          KpPlus        t             /**/t H J    K L ; t],
            [RShift     {NO}  {NO}   Kp1  Kp2       Kp3         t          KpEnter       t             /**/t N M    , . / t],
            [RCtrl      RAlt  t      t    t         t           t          t             t             /**/t t t    t t t t],
        }
        {               //    L_NUMBERS
            [t          {NO}  {NO}   {NO} Kb0       {NO}        t          t             t             /**/t t t    t t t W],
            [t          {NO}  {NO}   Kb9  Kb8       Kb7         t          t             t             /**/t t U    I O P t],
            [t          {NO}  {NO}   Kb4  Kb5       Kb6         t          t             t             /**/t H J    K L ; t],
            [t          {NO}  {NO}   Kb1  Kb2       Kb3         t          t             t             /**/t N M    , . / t],
            [t          t     t      t    t         t           t          t             t             /**/t t t    t t t t],
        }
        {               //    L_RIGHT
            [t          t     t      t    t         t           t          t             t             /**/t t t    t t t W],
            [t          Y     U      I    O         P           t          t             t             /**/t t U    I O P t],
            [t          H     J      K    L         ;           t          t             t             /**/t H J    K L ; t],
            [t          N     M      ,    .         /           t          t             t             /**/t N M    , . / t],
            [t          t     t      t    t         t           t          t             t             /**/t t t    t t t t],
        }
        {               //    L_SPECIAL
            [t          t     t      t    t         t           t          t             t             /**/t t t    t t t W],
            [t          t     t      t    t         t           t          t             t             /**/t t U    I O P t],
            [t          t     t      t    t         t           t          t             t             /**/t H J    K L ; t],
            [t          t     t      t    t         t           t          t             t             /**/t N M    , . / t],
            [t          t     t      t    t         t           t          t             t             /**/t t t    t t t t],
        }
        //              {
        //              [t    t      t    t         t           t          t             t             t     t /**/ t t t t t t],
        //              [t    t      t    t         t           t          t             t             t     t /**/ t t t t t t],
        //              [t    t      t    t         t           t          t             t             t     t /**/ t t t t t t],
        //              [t    t      t    t         t           t          t             t             t     t /**/ t t t t t t],
        //              [t    t      t    t         t           t          t             t             t     t /**/ t t t t t t],
        //              }
    };
}

pub use keymap::LayoutT;
pub use keymap::LAYERS;
